<?php

namespace PM\Presentation\Report;

use PM\Application\Report\ReportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ReportController extends AbstractController
{
    /** @var \PM\Application\Report\ReportRepository */
    private $repository;

    public function __construct(ReportRepository $repository)
    {
        $this->repository = $repository;
    }

    public function reportA(): Response
    {
        $report = $this->repository->getReportA();

        return $this->render('report/reportA.html.twig', [
            'report' => $report
        ]);
    }

    public function reportB(): Response
    {
        $report = $this->repository->getReportB();

        return $this->render('report/reportB.html.twig', [
            'report' => $report
        ]);
    }
}