<?php

namespace PM\Application\Owner;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use PM\Domain\Owner\Owner;

class OwnerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Owner::class);
    }

    /**
     * @return \PM\Domain\Owner\Owner[]
     */
    public function findAllOwnersWithPetsOrderedByNames(): array
    {
        $qb = $this->createQueryBuilder('o')
            ->select('o, p')
            ->leftJoin('o.pets', 'p')
            ->addOrderBy('o.name', 'ASC')
            ->addOrderBy('o.surname', 'ASC')
            ->addOrderBy('p.name', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findAllOrderedByNumberOfPets(): array
    {
        $sql = <<< HEREDOC
            SELECT o.name , o.surname, count(p.id) as numberOfPets
            FROM owner as o
            LEFT JOIN pet as p ON o.id = p.owner_id
            GROUP BY o.id
            ORDER BY numberOfPets DESC
HEREDOC;

        $statement = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        $statement->execute();

        return $statement->fetchAll();
    }
}