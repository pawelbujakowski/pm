<?php

namespace PM\Presentation\Owner;

use PM\Application\Owner\OwnerRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OwnersType extends AbstractType
{
    /** @var \PM\Application\Owner\OwnerRepository */
    private $repository;

    public function __construct(OwnerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        /** @var \PM\Domain\Owner\Owner[] $owners */
        $owners = $this->repository->findAll();

        $choices = [];
        foreach ($owners as $owner) {
            $choices[$owner->getFUllName()] = $owner->getId();
        }

        $resolver->setDefaults([
            'choices' => $choices,
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
