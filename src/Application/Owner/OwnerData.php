<?php

namespace PM\Application\Owner;

class OwnerData
{
    /** @var string */
    public $name;

    /** @var string */
    public $surname;
}