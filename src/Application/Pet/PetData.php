<?php

namespace PM\Application\Pet;

class PetData
{
    /** @var int */
    public $ownerId;

    /** @var string */
    public $name;

    /** @var string */
    public $specie;
}