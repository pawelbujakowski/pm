<?php

namespace PM\Application\Owner;

use Doctrine\ORM\EntityManagerInterface;
use PM\Domain\Owner\Owner;
use PM\Domain\Owner\OwnerAlreadyExistException;

class AddOwnerService
{
    /** @var \PM\Application\Owner\OwnerRepository */
    private $repository;

    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $entityManager;


    public function __construct(OwnerRepository $repository, EntityManagerInterface $entityManager)
    {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
    }

    public function add(OwnerData $data): Owner
    {
        $owner = $this->repository->findOneBy([
            'name' => $data->name,
            'surname' => $data->surname
        ]);

        if ($owner) {
            throw new OwnerAlreadyExistException();
        }

        $owner = new Owner($data->name, $data->surname);
        $this->entityManager->persist($owner);
        $this->entityManager->flush();

        return $owner;
    }
}