<?php

namespace PM\Application\Report;

use PM\Application\Owner\OwnerRepository;
use PM\Domain\Report\Report;
use PM\Domain\Report\ReportItem;

class ReportRepository
{
    /** @var \PM\Application\Owner\OwnerRepository */
    private $ownerRepository;

    public function __construct(OwnerRepository $ownerRepository)
    {
        $this->ownerRepository = $ownerRepository;
    }

    public function getReportA(): Report
    {
        $report = new Report();
        $owners = $this->ownerRepository->findAllOwnersWithPetsOrderedByNames();
        foreach ($owners as $owner) {
            $item = new ReportItem();
            $item->name = $owner->getName();
            $item->surname = $owner->getSurname();
            $item->pets = $owner->getPets()->toArray();

            $report->addItem($item);
        }

        return $report;
    }

    public function getReportB(): Report
    {
        $report = new Report();
        $owners = $this->ownerRepository->findAllOrderedByNumberOfPets();
        foreach ($owners as $owner) {
            $item = new ReportItem();
            $item->name = $owner['name'];
            $item->surname = $owner['surname'];
            $item->numberOfPets = $owner['numberOfPets'];

            $report->addItem($item);
        }


        return $report;
    }
}