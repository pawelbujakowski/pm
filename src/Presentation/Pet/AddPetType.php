<?php

namespace PM\Presentation\Pet;

use PM\Presentation\Owner\OwnersType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AddPetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ownerId', OwnersType::class)
            ->add('name', TextType::class)
            ->add('specie', TextType::class)
            ->add('save', SubmitType::class);
    }
}