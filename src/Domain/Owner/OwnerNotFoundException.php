<?php

namespace PM\Domain\Owner;

use RuntimeException;

class OwnerNotFoundException extends RuntimeException
{
}