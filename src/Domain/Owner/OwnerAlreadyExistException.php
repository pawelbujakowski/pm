<?php

namespace PM\Domain\Owner;

use RuntimeException;

class OwnerAlreadyExistException extends RuntimeException
{
}