<?php

namespace PM\Application\Pet;

use Doctrine\ORM\EntityManagerInterface;
use PM\Application\Owner\OwnerRepository;
use PM\Domain\Owner\OwnerNotFoundException;
use PM\Domain\Pet\Pet;
use PM\Domain\Pet\PetAlreadyExistException;

class AddPetService
{
    /** @var \PM\Application\Pet\PetRepository */
    private $repository;

    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $entityManager;

    /** @var \PM\Application\Owner\OwnerRepository */
    private $ownerRepository;

    public function __construct(
        PetRepository $petRepository,
        OwnerRepository $ownerRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->repository = $petRepository;
        $this->entityManager = $entityManager;
        $this->ownerRepository = $ownerRepository;
    }

    public function add(PetData $data): Pet
    {
        /** @var \PM\Domain\Owner\Owner $owner */
        $owner = $this->ownerRepository->find($data->ownerId);
        if (!$owner) {
            throw new OwnerNotFoundException();
        }

        $pet = $this->repository->findOneBy([
            'name' => $data->name,
            'owner' => $owner
        ]);

        if ($pet) {
            throw new PetAlreadyExistException();
        }

        $pet = new Pet($data->name, $data->specie);
        $pet->setOwner($owner);


        $this->entityManager->persist($pet);
        $this->entityManager->flush();

        return $pet;
    }
}