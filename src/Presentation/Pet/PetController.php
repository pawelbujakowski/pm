<?php

namespace PM\Presentation\Pet;

use PM\Application\Owner\OwnerRepository;
use PM\Application\Pet\AddPetService;
use PM\Application\Pet\PetData;
use PM\Application\Pet\PetRepository;
use PM\Domain\Pet\PetAlreadyExistException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PetController extends AbstractController
{
    /** @var \PM\Application\Pet\AddPetService */
    private $addPetService;

    /** @var \PM\Application\Pet\PetRepository */
    private $repository;

    /** @var \PM\Application\Owner\OwnerRepository */
    private $ownerRepository;

    public function __construct(
        AddPetService $addPetService,
        PetRepository $repository,
        OwnerRepository $ownerRepository
    ) {
        $this->addPetService = $addPetService;
        $this->repository = $repository;
        $this->ownerRepository = $ownerRepository;
    }

    public function index(): Response
    {
        $pets = $this->repository->findAll();

        return $this->render('pet/index.html.twig', [
            'pets' => $pets
        ]);
    }

    public function add(Request $request): Response
    {
        $owners = $this->ownerRepository->findAll();
        if (!count($owners)) {
            $this->addFlash(
                'danger',
                sprintf('To add a pet you need to have at least one owner defined')
            );

            return $this->redirectToRoute('pet_list');
        }

        $form = $this->createForm(AddPetType::class, new PetData());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $petData = $form->getData();
            try {
                $owner = $this->addPetService->add($petData);
                $this->addFlash(
                    'success',
                    sprintf('Pet %s has been added!', $owner->getName())
                );

                return $this->redirectToRoute('pet_list');

            } catch (PetAlreadyExistException $e) {
                $this->addFlash(
                    'danger',
                    'Pet already exist'
                );
            }
        }

        return $this->render('pet/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}