<?php

namespace PM\Domain\Pet;

use Doctrine\ORM\Mapping as ORM;
use PM\Domain\Owner\Owner;

/**
 * @ORM\Entity(repositoryClass="PM\Application\Pet\PetRepository")
 */
class Pet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $specie;

    /**
     * @ORM\ManyToOne(targetEntity="PM\Domain\Owner\Owner", inversedBy="pets")
     */
    private $owner;

    public function __construct(string $name, string $specie)
    {
        $this->name = $name;
        $this->specie = $specie;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSpecie(): string
    {
        return $this->specie;
    }

    public function getOwner(): ?Owner
    {
        return $this->owner;
    }

    public function setOwner(Owner $owner): void
    {
        $this->owner = $owner;
    }
}