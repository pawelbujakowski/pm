<?php

namespace PM\Presentation\Owner;

use PM\Application\Owner\AddOwnerService;
use PM\Application\Owner\OwnerData;
use PM\Application\Owner\OwnerRepository;
use PM\Domain\Owner\OwnerAlreadyExistException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OwnerController extends AbstractController
{
    /** @var \PM\Application\Owner\AddOwnerService  */
    private $addOwnerService;

    /** @var \PM\Application\Owner\OwnerRepository  */
    private $repository;

    public function __construct(
        AddOwnerService $addOwnerService,
        OwnerRepository $repository
    ) {
        $this->addOwnerService = $addOwnerService;
        $this->repository = $repository;
    }

    public function index(): Response
    {
        $owners = $this->repository->findAll();

        return $this->render('owner/index.html.twig', [
            'owners' => $owners
        ]);
    }

    public function add(Request $request): Response
    {
        $form = $this->createForm(AddOwnerType::class, new OwnerData());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ownerData = $form->getData();
            try {
                $owner = $this->addOwnerService->add($ownerData);
                $this->addFlash(
                    'success',
                    sprintf('Owner %s %s has been added!', $owner->getName(), $owner->getSurname())
                );

                return $this->redirectToRoute('owner_list');

            } catch (OwnerAlreadyExistException $e) {
                $this->addFlash(
                    'danger',
                    'Owner already exist'
                );
            }
        }

        return $this->render('owner/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}