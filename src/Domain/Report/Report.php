<?php

namespace PM\Domain\Report;

use ArrayIterator;
use IteratorAggregate;

class Report implements IteratorAggregate
{
    private $items = [];

    public function addItem(ReportItem $item): void
    {
        $this->items[] = $item;
    }

    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }
}