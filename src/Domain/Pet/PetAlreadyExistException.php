<?php

namespace PM\Domain\Pet;

use RuntimeException;

class PetAlreadyExistException extends RuntimeException
{
}